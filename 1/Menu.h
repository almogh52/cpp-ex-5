#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <string>

#define MAIN_MENU_OPTIONS_AMOUNT 4

#define CIRCLE_OPTION 0
#define ARROW_OPTION 1
#define TRIANGLE_OPTION 2
#define RECTANGLE_OPTION 3
#define SHAPE_MENU_OPTIONS_AMOUNT 4

#define ARROW_POINTS_AMOUNT 2
#define TRIANGLE_POINTS_AMOUNT 3

#define MOVE_SHAPE 0
#define SHAPE_DETAILS 1
#define REMOVE_SHAPE 2
#define SHAPE_MODIFY_OPTIONS_AMOUNT 3

#define EMPTY -1

class Menu
{
public:

	Menu();
	~Menu();

	int mainMenu() const;
	void newShape();
	void redrawShapes() const;
	int showShapesMenu() const;
	void modifyShape(int index);
	void removeAll();
	std::vector<Point> getPoints(int amount);

private:
	std::vector<Shape *> _shapes;

	int showMenu(const std::string choices[], int amount) const;

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

