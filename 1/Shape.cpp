#include "Shape.h"

/**
* This is the main constructor of the Shape class.
* param type: The name of the shape
* param name: The type of the shape
**/
Shape::Shape(const string & name, const string & type) : _type(type), _name(name) {}

/**
* This is the destructor of the Shape class.
**/
Shape::~Shape() 
{
	// Clear the vector of CImg
	_graphicShape.clear();
}

/**
* This function prints the details of the shape
* return: None
**/
void Shape::printDetails() const
{
	// Print the name and the type of the shape
	std::cout << "Name: " << _name << std::endl;
	std::cout << "Type: " << _type << std::endl;

	// Print the area and perimeter of the shape
	std::cout << "Area: " << this->getArea() << std::endl;
	std::cout << "Perimeter: " << this->getPerimeter() << std::endl;
}

// Getters
string Shape::getType() const
{
	return _type;
}

string Shape::getName() const
{
	return _name;
}
