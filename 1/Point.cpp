#include "Point.h"

/**
* This is the main constructor of the Point class.
* param x: The x value of the point
* param y: The y value of the point
**/
Point::Point(double x, double y) : _x(x), _y(y) {}

/**
* This is the copy constructor of the Point class.
* param other: The reference to the other Point object to be copied from
**/
Point::Point(const Point & other)
{
    // Copy fields from the other point object
    _x = other._x;
    _y = other._y;
}

/**
* This operator adds 2 points together
* param other: The reference to the other Point object to add to this object
* return: The point that represent the addition of the 2 points
**/
Point Point::operator+(const Point & other) const
{
    // Duplicate this object
    Point pCopy = Point(*this);

    // Add the other point object to the duplicate using the += operator
    pCopy += other;

    return pCopy;
}

/**
* This operator adds to this point another point
* param other: The reference to the other Point object to add to this object
* return: The reference to the new point
**/
Point & Point::operator+=(const Point & other)
{
    // Add the coordinates of the other point to this point
    _x += other._x;
    _y += other._y;

    return *this;
}

// Getters
double Point::getX() const
{
    return _x;
}

double Point::getY() const
{
    return _y;
}

/**
* This function calculates distance from another point
* param other: The reference to the other Point object to calc distance from
* return: The distance between the 2 points
**/
double Point::distance(const Point & other) const
{
    // Calculate distance of the 2 points using the distance formula and the cmath library
    return sqrt(pow(_x - other._x, 2) + pow(_y - other._y, 2));
}
