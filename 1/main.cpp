#include "Menu.h"
#include "Rectangle.h"
#include <iostream>

#define MENU_NEW_SHAPE    0
#define MENU_MODIFY_SHAPE 1
#define MENU_DELETE_ALL   2
#define MENU_EXIT         3

int main()
{
	int choice = 0, shapeChoice = 0;
	bool exit = false;

	// Create the menu handler
    Menu menu = Menu();

	// While we shouldn't close the program and exit
    while (!exit)
    {
		// Get the choice from the user after printing the main menu
		choice = menu.mainMenu();

		// Handle the user's choice
		switch (choice)
		{
		case MENU_NEW_SHAPE:
			// Call the new shape menu
			menu.newShape();
			break;

		case MENU_MODIFY_SHAPE:
			shapeChoice = menu.showShapesMenu();

			// If the vector isn't empty
			if (shapeChoice != EMPTY)
			{
				// Modify the wanted shape
				menu.modifyShape(shapeChoice);
			}
			break;

		case MENU_DELETE_ALL:
			// Remove all shapes
			menu.removeAll();
			break;

		case MENU_EXIT:
			// Set the program to be exited
			exit = true;
			break;
		}

		// Redraw shapes every time
		menu.redrawShapes();
    }

	// Remove all the shapes after the exit
	menu.removeAll();

	return 0;
}