#include "Menu.h"

/**
* This is the main constructor of the Menu class.
**/
Menu::Menu() 
{
	// Create board and display from board
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

/**
* This is the destructor of the Menu class.
**/
Menu::~Menu()
{
	// Close the display and free memory from board and display
	_disp->close();
	delete _board;
	delete _disp;
}

/**
* This function prints the main menu
* return: The choice selected
**/
int Menu::mainMenu() const
{
	// Create the choices array for the main menu
	const std::string choices[] = {
		"add a new shape.",
		"modify or get information from a current shape.",
		"delete all of the shapes.",
		"exit."
	};

	// Print the menu and get the choice from the user
	return this->showMenu(choices, MAIN_MENU_OPTIONS_AMOUNT);
}

/**
* This function shows the new shape menu and creates a new shape
* return: None
**/
void Menu::newShape()
{
	int choice = 0;

	std::string type, name;
	int x = 0, y = 0, radius = 0, length = 0, width = 0;
	std::vector<Point> points;

	// Create the choices array for the shape menu
	const std::string choices[] = {
		"add a circle.",
		"add an arrow.",
		"add a triangle.",
		"add a rectangle."
	};

	// Show the menu to the user and get it's choice
	choice = this->showMenu(choices, SHAPE_MENU_OPTIONS_AMOUNT);

	// Handle the shape choice
	switch (choice)
	{
	case CIRCLE_OPTION:
		// Set the type of the new shape
		type = "Circle";

		// Get center x
		std::cout << "Please enter center point X: ";
		std::cin >> x;

		// Get center y
		std::cout << "Please enter center point Y: ";
		std::cin >> y;

		// Get circle radius
		std::cout << "Please enter radius: ";
		std::cin >> radius;

		// Get the name of the shape
		std::cout << "Please enter the name of the shape: ";
		std::cin >> name;

		// Create the circle and push it to the vector of shapes
		_shapes.push_back(new Circle(Point(x, y), radius, type, name));
		break;

	case ARROW_OPTION:
		// Set the type of the new shape
		type = "Arrow";

		// Get 2 points from the user
		points = getPoints(ARROW_POINTS_AMOUNT);

		// Get the name of the shape
		std::cout << "Please enter the name of the shape: ";
		std::cin >> name;

		// Create the arrow and push it to the vector of shapes
		_shapes.push_back(new Arrow(points[0], points[1], type, name));
		break;

	case TRIANGLE_OPTION:
		// Set the type of the new shape
		type = "Triangle";

		// Get 3 points from the user
		points = getPoints(TRIANGLE_POINTS_AMOUNT);

		// Get the name of the shape
		std::cout << "Please enter the name of the shape: ";
		std::cin >> name;

		// If the base vertices has the same Y axis as the other vertex, send error
		if (points[0].getY() == points[1].getY() || points[2].getY() == points[1].getY())
		{
			// Print error and pause
			std::cout << "The triangle you entered was invalid!" << std::endl;
			system("pause");
		}
		else {
			// Create the triangle and push it to the vector of shapes
			_shapes.push_back(new Triangle(points[0], points[1], points[2], type, name));
		}
		break;

	case RECTANGLE_OPTION:
		// Set the type of the new shape
		type = "Rectangle";

		// Get left corner x
		std::cout << "Please enter left corner point X: ";
		std::cin >> x;

		// Get left corner y
		std::cout << "Please enter left corner point Y: ";
		std::cin >> y;

		// Get rectangle length
		std::cout << "Please enter length of the rectangle: ";
		std::cin >> length;

		// Get rectangle width
		std::cout << "Please enter width of the rectangle: ";
		std::cin >> width;

		// Get the name of the shape
		std::cout << "Please enter the name of the shape: ";
		std::cin >> name;

		// If the length or width of the rectangle are 0 send error
		if (length == 0 || width == 0)
		{
			// Print error and pause
			std::cout << "The rectangle you entered was invalid!" << std::endl;
			system("pause");
		}
		else {
			// Create the rectangle and push it to the vector of shapes
			_shapes.push_back(new myShapes::Rectangle(Point(x, y), length, width, type, name));
		}
		break;
	}
}

/**
* This function redraws all the shapes
* return: None
**/
void Menu::redrawShapes() const
{
	// For each shape, clear it's draw from the board
	for (Shape *shape : _shapes)
	{
		// Clear draw for shape
		shape->clearDraw(*_disp, *_board);
	}

	// For each shape, draw it to the board
	for (Shape *shape : _shapes)
	{
		// Draw the shape
		shape->draw(*_disp, *_board);
	}
}

/**
* This function the current shapes menu and get's a shape from the user
* return: The shapes selected
**/
int Menu::showShapesMenu() const
{
	std::vector<std::string> choices;

	// If the shapes vector isn't empty, show vector menu
	if (_shapes.size() != 0)
	{
		// Add each shape as a choice
		for (Shape *shape : _shapes)
		{
			ostringstream formatter;

			// Format the choice with the name and type of the shape
			formatter << "modify " << shape->getType() << "(" << shape->getName() << ")";

			// Convert the stream to string and push it to the vector
			choices.push_back(formatter.str());
		}

		// Convert the vector to array using simple trick and show the choices to the user
		return this->showMenu(&choices[0], _shapes.size());
	}

	return EMPTY;
}

/**
* This function modifies a specific shape
* param index: The index of the shape
* return: None
**/
void Menu::modifyShape(int index)
{
	int choice = 0, x = 0, y = 0;


	// Create the choices array for the shape modify menu
	const std::string choices[] = {
		"move the shape.",
		"get it's details.",
		"remove the shape.",
	};

	// Print the menu and get the choice from the user
	choice = this->showMenu(choices, SHAPE_MODIFY_OPTIONS_AMOUNT);
	
	// Handle the user's choice
	switch (choice)
	{
	case MOVE_SHAPE:
		// Clear the screen
		system("cls");

		// Get x moving scale
		std::cout << "Please enter X moving scale: ";
		std::cin >> x;

		// Get y moving scale
		std::cout << "Please enter Y moving scale: ";
		std::cin >> y;

		// Clear it's draw before moving
		_shapes[index]->clearDraw(*_disp, *_board);

		// Move the shape using the moving scale point
		_shapes[index]->move(Point(x, y));

		break;

	case SHAPE_DETAILS:
		// Clear the screen
		system("cls");

		// Print the shape's details
		_shapes[index]->printDetails();

		// Wait for key press
		system("pause");
		break;

	case REMOVE_SHAPE:
		// Clear the shape draw before delete
		_shapes[index]->clearDraw(*_disp, *_board);

		// Free the shape
		delete _shapes[index];

		// Remove the element in the index from the vector
		_shapes.erase(_shapes.begin() + index);

		break;
	}
}

/**
* This function removes all the shapes
* return: None
**/
void Menu::removeAll()
{
	// Get vector size
	int size = _shapes.size();

	for (int i = 0; i < _shapes.size(); i++)
	{
		// Clear the shape draw before delete
		_shapes.back()->clearDraw(*_disp, *_board);

		// Free the element
		delete _shapes.back();

		// Remove the element from the vector
		_shapes.pop_back();
	}
}

/**
* This function get's points from the user
* param amount: The amount of points
* return: The vector with the points
**/
std::vector<Point> Menu::getPoints(int amount)
{
	int x = 0, y = 0;
	std::vector<Point> points;

	// Get points by the amount given
	for (int i = 0; i < amount; i++)
	{
		// Get point x
		std::cout << "Please enter point " << i + 1 << " X: ";
		std::cin >> x;

		// Get point y
		std::cout << "Please enter point " << i + 1 << " Y: ";
		std::cin >> y;

		// Create the point and save it in the vector
		points.push_back(Point(x, y));
	}

	return points;
}

/**
* This function shows the a menu to the user
* param choices: The choices for the user
* param amount: The amount of choices to the user
* return: The choice selected
**/
int Menu::showMenu(const std::string choices[], int amount) const
{
	int choice = 0;

	// Print the choices and ask for input while the choice selected is invalid
	do {
		// Clear the screen using system call
		system("cls");

		// Print all the choices
		for (int i = 0; i < amount; i++)
		{
			// Print the choice with it's index
			std::cout << "Enter " << i << " to " << choices[i] << std::endl;
		}

		// Get a choice from the user
		cin >> choice;
	} while (choice < 0 || choice >= amount); // Check for invalid choice

	return choice;
}

