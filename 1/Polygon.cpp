#include "Polygon.h"

/**
* This is the main constructor of the Polygon class.
* param type: The name of the polygon
* param name: The type of the polygon
**/
Polygon::Polygon(const string & type, const string & name) : Shape(name, type) {}

/**
* This is the destructor of the Polygon class.
**/
Polygon::~Polygon() 
{
	// Clear the vector of points
	_points.clear();
}

/**
* This function moves the polygon by a given point
* param other: The point that will count as offset for the polygon
* return: None
**/
void Polygon::move(const Point & other)
{
	// Go through the points, for each one add the other point to move it
	for (Point& point : _points)
	{
		// Add the other point to the current point
		point += other;
	}
}
