#include "Arrow.h"

/**
* This is the main constructor of the Arrow class.
* param a: The first point of the arrow
* param b: The second point of the arrow
* param type: The name of the arrow
* param name: The type of the arrow
**/
Arrow::Arrow(const Point & a, const Point & b, const string & type, const string & name) : Shape(name, type), _p1(a), _p2(b) {}

/**
* This is the destructor of the Arrow class.
**/
Arrow::~Arrow() {}

/**
* This function calculates the area of the arrow
* return: The area of the arrow
**/
double Arrow::getArea() const
{
	// Arrow has no area
	return 0.0;
}

/**
* This function calculates the perimeter of the arrow
* return: The perimeter of the arrow
**/
double Arrow::getPerimeter() const
{
	// The perimeter of the arrow is just the distance between the 2 points
	return _p1.distance(_p2);
}

/**
* This function moves the arrow by a given point
* param other: The point that will count as offset for the arrow
* return: None
**/
void Arrow::move(const Point & other)
{
	// Add the offset point to both points to move the arrow
	_p1 += other;
	_p2 += other;
}

/**
* This function draws the arrow on the screen
* param disp: The display to show on it the arrow
* param board: The board that holds the pixel values of the arrow
* return: None
**/
void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}

/**
* This function removes the arrow from the screen
* param disp: The display to remove from it the arrow
* param board: The board that holds the pixel values of the arrow
* return: None
**/
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


