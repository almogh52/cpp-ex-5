#include "Circle.h"

/**
* This is the main constructor of the Circle class.
* param center: The center point of the circle
* param radius: The radius of the circle
* param type: The name of the circle
* param name: The type of the circle
**/
Circle::Circle(const Point & center, double radius, const string & type, const string & name) : Shape(name, type), _center(center), _radius(radius) {}

/**
* This is the destructor of the Circle class.
**/
Circle::~Circle() {}

// Getters
const Point & Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}

/**
* This function calculates the area of the circle
* return: The area of the circle
**/
double Circle::getArea() const
{
	// Calc the area of the circle using the circle area formula
	return PI * pow(_radius, 2);
}

/**
* This function calculates the perimeter of the circle
* return: The perimeter of the circle
**/
double Circle::getPerimeter() const
{
	// Calc the perimeter of the circle using the circle perimeter formula
	return 2 * PI * _radius;
}

/**
* This function moves the circle by a given point
* param other: The point that will count as offset for the circle
* return: None
**/
void Circle::move(const Point & other)
{
	// Add the offset point to the center point of the circle in order to move it
	_center += other;
}

/**
* This function draws the circle on the screen
* param disp: The display to show on it the circle
* param board: The board that holds the pixel values of the circle
* return: None
**/
void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

/**
* This function removes the circle from the screen
* param disp: The display to remove from it the circle
* param board: The board that holds the pixel values of the circle
* return: None
**/
void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
}


