#include "Triangle.h"

/**
* This is the main constructor of the Triangle class.
* param a: The first vertex point
* param b: The second vertex point
* param c: The third vertex point
* param type: The name of the triangle
* param name: The type of the triangle
**/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : Polygon(type, name)
{
	// Push the 3 vertices points to the points vector
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

/**
* This is the destructor of the Triangle class.
**/
Triangle::~Triangle() {}

/**
* This function calculates the area of the triangle
* return: The area of the triangle
**/
double Triangle::getArea() const
{
	double base = 0, length = 0;

	// Calculate the base side which is the distance between the 2 base vertices
	base = _points[2].distance(_points[0]);

	// The length to the base is the top vertex y axis - the base vertex y axis
	length = abs(_points[1].getY() - _points[0].getY());

	// Calc triangle area by the normal formula
	return (base * length) / 2;
}

/**
* This function calculates the perimeter of the triangle
* return: The perimeter of the triangle
**/
double Triangle::getPerimeter() const
{
	// Return the addition of the length of all sides of the triangle
	return _points[0].distance(_points[1]) + // First side
		   _points[1].distance(_points[2]) + // Second side
		   _points[2].distance(_points[0]); // Third side
}

/**
* This function draws the triangle on the screen
* param disp: The display to show on it the triangle
* param board: The board that holds the pixel values of the triangle
* return: None
**/
void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

/**
* This function removes the triangle from the screen
* param disp: The display to remove from it the triangle
* param board: The board that holds the pixel values of the triangle
* return: None
**/
void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
