#include "Rectangle.h"

/**
* This is the main constructor of the Rectangle class.
* param a: The top left corner point
* param length: The length of the rectangle
* param width: The width of the rectangle
* param type: The name of the rectangle
* param name: The type of the rectangle
**/
myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(type, name)
{
	// Create a point with the length and width to be used as an offset point to create the second point
	Point offsetPoint = Point(width, length);

	// Push the first point of the rect to the points array
	_points.push_back(a);

	// Push the second point which is the first point + the offset point which is declared using the width and length
	_points.push_back(a + offsetPoint);
}

/**
* This is the destructor of the Rectangle class.
**/
myShapes::Rectangle::~Rectangle() {}

/**
* This function calculates the area of the rectangle
* return: The area of the rectangle
**/
double myShapes::Rectangle::getArea() const
{
	double length = 0, width = 0;

	// Get the length and width by substracting the second point coordinates from the first point
	length = _points[1].getY() - _points[0].getY();
	width = _points[1].getX() - _points[0].getX();

	// Return the area of the rectangle which is it's length multiply by it's width
	return length * width;
}

/**
* This function calculates the perimeter of the rectangle
* return: The perimeter of the rectangle
**/
double myShapes::Rectangle::getPerimeter() const
{
	double length = 0, width = 0;

	// Get the length and width by substracting the second point coordinates from the first point
	length = _points[1].getY() - _points[0].getY();
	width = _points[1].getX() - _points[0].getX();

	// Return the perimeter of the rectangle which is the length multiply by 2 (2 sides of rect) and width multiply by 2 (2 other sides of rect)
	return length * 2 + width * 2;
}

/**
* This function draws the rectangle on the screen
* param disp: The display to show on it the rectangle
* param board: The board that holds the pixel values of the rectangle
* return: None
**/
void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

/**
* This function removes the rectangle from the screen
* param disp: The display to remove from it the rectangle
* param board: The board that holds the pixel values of the rectangle
* return: None
**/
void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


